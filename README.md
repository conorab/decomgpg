# decomgpg

This script allows you to ensure files which are signed or encrypted with GPG are first decrypted or verified, save the current hash of the files they refer to and then delete the no-longer-needed '.gpg' and '.sig' files. In other words, this script assists in migrating away from using GPG in favour of just keeping SHA256 hashes of those files. This is particularly useful when you wish to revoke a key but ensure you don't lose access to data which was signed by or encrypted to that key.

## Defintions/Variables

* grepMatch - The string to match in the SHA256 line.
* sourceSHAPath - The SHA256 file check through.
* newSHAFile - The file to add the newly-generated SHA256 sums to.
* removedSHAFile - The file to add the SHA256 sums being removed from sourceSHAPath after successfully verifying a hash.
* sourceFilename - The file currently being processed from the in the 'sourceSHAPath' file.
* destFilename - The file with the same name and path as 'sourceFilename' but with the '.gpg' or '.sig' extension at the end removed.
* tmpDestFilename - A temporary file created to store decrypted contents of 'sourceFilename'.

## Explanation of Script Logic

1. 'decomgpg' greps for the string in the 'grepMatch' variable in the file referenced in the 'sourceSHAPath' file.
2. Another grep is done to match only lines which refer to files ending in '.gpg' or '.sig'.
3. As each hash/file is interated through, the file name is set in the 'sourceFilename' variable and the 'destFilename' is derived from this. A new empty file with a random name is  also created and its path is stored in 'tmpDestFilename'.
1. A SHA256 hash of the file referenced in 'sourceFilename' is generated and compared to the hash in the current line. If these hashes do not match then this line is skipped (return to step 3).
1. GPG is instructed to decrypt the file referenced in 'sourceFilename' and save the decrypted contents in 'tmpDestFilename'. If the the decryption/verification fails, this line is skipped (return to step 3).
   * If the file referenced in 'tmpDestFilename' is empty, then the file being decrypted was a signature and so the file referenced in 'tmpDestFilename' is deleted.
   * If the file referenced in 'tmpDestFilename' is not empty and the file referenced in 'destFilename' exists, then these two files are compared. If they have the same contents, then the file referenced in 'tmpDestFilename' is deleted.
      * If these files are different, then the file referenced in 'tmpDestFilename' is deleted and this line is skipped (return to step 3).
   * If the file referenced in 'tmpDestFilename' is not empty and the file referenced in 'destFilename' does not exist, then the temporary file is renamed to the contents of 'DestFilename'.
1. The SHA256 of the file referenced in 'DestFilename' is generated. If this fails and 'destFilename' was the result of 'tmpDestFilename' being renamed, then 'destFilename' is deleted and this line is skipped (return to step 3). If it was not the result of a rename, 'destFilename' is kept.
1. The SHA256 is appended to the file referenced in 'newSHAFile'.
1. The current line is appended to the file referenced in 'removedSHAFile'.
1. The current line is removed from the file referenced in 'sourceSHAPath'.
1. The file referenced in 'sourceFilename' is deleted. The last 4 steps (including this one) are done in this order so that a fatal error  will at worst lead to a duplicate line in the files referenced in 'newSHAFile', 'removedSHAFile' and/or 'sourceSHAPath' rather than a loss of data.